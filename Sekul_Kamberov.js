"use strict";

var _get = function get(object, property, receiver) {
  var desc = Object.getOwnPropertyDescriptor(object, property);
  if (desc === undefined) {
    var parent = Object.getPrototypeOf(object);
    if (parent === null) {
      return undefined;
    } else {
      return get(parent, property, receiver);
    }
  } else if ("value" in desc && desc.writable) {
    return desc.value;
  } else {
    var getter = desc.get;
    if (getter === undefined) {
      return undefined;
    }
    return getter.call(receiver);
  }
};

var _inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }
  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) subClass.__proto__ = superClass;
};

var _prototypeProperties = function (child, staticProps, instanceProps) {
  if (staticProps) Object.defineProperties(child, staticProps);
  if (instanceProps) Object.defineProperties(child.prototype, instanceProps);
};

var _classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/**
 * Base class example
 */
var Person = (function () {
  function Person(name) {
    _classCallCheck(this, Person);

    this.name = name;
    console.log("creating a new person");
  }

  _prototypeProperties(Person, null, {
    hello: {
      value: function hello() {
        console.log("Hello " + this.name + "! You're a person!");
      },
      writable: true,
      configurable: true
    }
  });

  return Person;
})();

/**
 * Derived class example
 */
var Worker = (function (Person) {
  function Worker(name, job) {
    _classCallCheck(this, Worker);

    _get(Object.getPrototypeOf(Worker.prototype), "constructor", this).call(this, name);
    this.job = job;
    console.log("creating a new worker");
  }

  _inherits(Worker, Person);

  _prototypeProperties(Worker, null, {
    hello: {
      value: function hello() {
        console.log("Hello " + this.name + "! You're a worker!");
      },
      writable: true,
      configurable: true
    }
  });

  return Worker;
})(Person);

/*
 * Instantiation & usage
 */
var p1 = new Person("Jack");
var p2 = new Person("Juliet");
var w1 = new Worker("Jacob");

p1.hello();
p2.hello();
w1.hello();